# [UniverseHistory](https://bitbucket.org/steog88/universehistory_public/)
by S. Gariazzo (stefano.gariazzo@gmail.com) and I. Maturana (ivania.daniela@gmail.com)

An image showing the evolution of the Universe from the Big Bang until today, highlighting some important events and the particles that were present in each moment.  
Designed for use in talks or similar.

## Acknowledgements
The background and additional images are taken from the web, in particular we use:

* a Cosmic Microwave Background image from [Planck](https://www.esa.int/Planck), a project of the European Space Agency (ESA);
* a cosmic web image extracted from the [Millennium Simulation Project](https://wwwmpa.mpa-garching.mpg.de/galform/virgo/millennium/);
* a black hole picture from the [Event Horizon Telescope](https://eventhorizontelescope.org/);
* several images from the [Hubble Space Telescope](https://hubblesite.org/), including a star, a galaxy and a picture of the sky.
